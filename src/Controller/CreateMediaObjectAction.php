<?php

namespace App\Controller;

use App\Entity\MediaObject;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Vich\UploaderBundle\Entity\File;

#[AsController]
final class CreateMediaObjectAction extends AbstractController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager): MediaObject
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        $authorString = $request->get('author');

        $publicDir = $this->getParameter('kernel.project_dir') . '/public/';
        $overlaySrc = $publicDir . 'assets/pizza-slice.png';

        if (!file_exists($overlaySrc)) {
            throw new BadRequestHttpException('"Overlay file" ' . $overlaySrc . ' not found',);
        }

        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        if (!$authorString) {
            throw new BadRequestHttpException('"Author" is required');
        }

        $mediaObject = new MediaObject();
        $authorArray = explode('/',$authorString);
        $userId = $authorArray[3];

        $user = $entityManager->getRepository(User::class)->find($userId);
        list($overlayWidth, $overlayHeight) = getimagesize($overlaySrc);
        $overlay = imagecreatefrompng($overlaySrc);
        $base = imagecreatefrompng($uploadedFile->getRealPath());

        imagesavealpha($overlay, true);
        imagecopy($base, $overlay, 0, 0, 0, 0, $overlayWidth, $overlayHeight);

        header('Content-type: image/png');
        imagepng($base, $uploadedFile->getRealPath());

        $file = new UploadedFile($uploadedFile->getRealPath(), 'processed.png', 'image/png');
        $mediaObject->setFile($file);
        $mediaObject->setAuthor($user);

        imagedestroy($overlay);
        imagedestroy($base);

        return $mediaObject;
    }

}
