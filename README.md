# Symfony Docker

A [Docker](https://www.docker.com/)-based installer and runtime for the [Symfony](https://symfony.com) web framework, with full [HTTP/2](https://symfony.com/doc/current/weblink.html), HTTP/3 and HTTPS support.

![CI](https://github.com/dunglas/symfony-docker/workflows/CI/badge.svg)

## Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)
2. Run `docker-compose build --pull --no-cache` to build fresh images
3. Run `docker-compose up` (the logs will be displayed in the current shell)
4. Run `docker-compose exec php sh -c '
   set -e
   apk add openssl
   php bin/console lexik:jwt:generate-keypair
   setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
   setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
   '` to setup jwt public and private keys
5. Run `docker-compose exec php bin/console doctrine:fixtures:load` to create demo user
6. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)
7. Run `docker-compose down --remove-orphans` to stop the Docker containers.

**Enjoy!**

## Credits

Created by [Kévin Dunglas](https://dunglas.fr), co-maintained by [Maxime Helias](https://twitter.com/maxhelias) and sponsored by [Les-Tilleuls.coop](https://les-tilleuls.coop).
